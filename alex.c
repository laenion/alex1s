// Alex the Allegator

#define VERSION_STR      "0.1a"

// #define alleg_joystick_unused
#define alleg_vidmem_unused
#define alleg_flic_unused
#define alleg_math_unused
#define alleg_gui_unused
#define alleg_mouse_unused
// #define alleg_sound_unused
// #define alleg_timer_unused
// #define alleg_graphics_unused
// #define alleg_file_unused
// #define alleg_keyboard_unused
// #define alleg_gfx_driver_unused
// #define alleg_palette_unused
// #define alleg_datafile_unused

#include <stdio.h>

#include "allegro.h"                    // allegro
#include "ansicodes.h"
#include "comptime.h"                   // displays compilationtime
#include "gfx.h"                        // datfile
#include "sound.h"                      // datfile

// Shrink the EXE size some on DOS
// On Linux the library is linked dynamically anyway
#ifdef GFX_DRIVER_VGA
BEGIN_GFX_DRIVER_LIST
  GFX_DRIVER_VGA
END_GFX_DRIVER_LIST
BEGIN_COLOR_DEPTH_LIST
  COLOR_DEPTH_8
END_COLOR_DEPTH_LIST
#endif

void   __crt0_load_environment_file(char *_app_name) { }
char **__crt0_glob_function(char *_arg) { return 0; }
void   __crt0_setup_arguments();

DATAFILE *gfx;
DATAFILE *snd;
BITMAP *swap_screen;
BITMAP *bkg_screen;
BITMAP *view1;
BITMAP *view2;

int players;

#include "level.c"
#include "player.c"

// number of current screenshot
int screen_shot=0;

// timer variables
volatile int frame_count;
volatile int fps;
volatile int game_count;

void fps_counter()
{
  fps=frame_count;
  frame_count=0;
}
END_OF_FUNCTION(fps_counter);

void game_counter()
{
  game_count++;
}
END_OF_FUNCTION(game_counter);

int take_screenshot()
{
  BITMAP *bmp;
  PALETTE p;
  char filename[32];

  // don't take too many -- should never be a real problem
  if (screen_shot > 999) return 0;

  bmp=create_sub_bitmap(screen,0,0,SCREEN_W,SCREEN_H);

  if (!bmp) return 0;

  get_palette(p);

  sprintf(filename,"alex_%03d.pcx",screen_shot);
  screen_shot++;

  if (save_pcx(filename,bmp,p))
  {
    destroy_bitmap(bmp);
    return 1;
  }
  else
  {
    destroy_bitmap(bmp);
    return 0;
  }
}

void reset_keys()
{
  pkey[0].up      = KEY_UP;
  pkey[0].down    = KEY_DOWN;
  pkey[0].right   = KEY_RIGHT;
  pkey[0].left    = KEY_LEFT;
  pkey[0].jump    = KEY_RCONTROL;
  pkey[0].action  = KEY_ALTGR;

  pkey[1].up      = KEY_W;
  pkey[1].down    = KEY_S;
  pkey[1].right   = KEY_D;
  pkey[1].left    = KEY_A;
  pkey[1].jump    = KEY_F;
  pkey[1].action  = KEY_G;
}

void pause_game()
{
  int i=0;
  int x,y;

  midi_pause();
  while(!key[KEY_PAUSE]);
  for(x=0;x<320;x++)
  {
    i++; if (i==2) i=0;
    for(y=0;y<200;y++)
    {
      if (i) _putpixel(screen,x,y,0);
      i++; if (i==2) i=0;
    }
  }
  textout_centre(screen,gfx[FNT_MARKER].dat,"GAME PAUSED",160,80,-1);
  while(key[KEY_PAUSE]);
  midi_resume();
}

void prog_star(int pos)
{
  if ((pos & 3) == 3)
  {
    printf("■");
    fflush(stdout);
  }
}

void load_data()
{
  char buf[80];

  strcpy(buf, "gfx.dat");
  gfx = load_datafile(buf);
  if (!gfx)
  {
    allegro_exit();
    printf("Error Loading: GFX.DAT\nPlease reinstall.\n\n");
    exit(1);
  }

  strcpy(buf, "sound.dat");
  snd = load_datafile(buf);
  if (!snd)
  {
    allegro_exit();
    printf("Error Loading: SND.DAT\nPlease reinstall.\n\n");
    exit(1);
  }
}

void init()
{
  int i;

  printf(CLEAR_SCREEN);

  printf(FG_BRIGHT_WHITE BG_GREEN);
  printf("█▓▒░  Alex the Allegator                       Copyright 1999  Johan Peitz  " FG_BLACK "░▒▓█\n");
  printf(FG_WHITE BG_DEFAULT "\n");
  printf(" Version %s\n",VERSION_STR);
  printf(" Compiled: %s\n",COMP_TIME);
  printf(" Design, code and graphics: Johan Peitz\n");
  printf(" Allegro %s, by Shawn Hargreaves, %s\n",ALLEGRO_VERSION_STR,ALLEGRO_DATE_STR);
  fflush(stdout);

  printf(FG_GREEN BG_DEFAULT "\n");
  printf("════[" FG_BRIGHT_GREEN "SETTING UP" FG_GREEN "]════════════════════════════════════════════════════════════════\n");

  allegro_init();

  printf(FG_WHITE "\n");
  printf(" Installing Keyboard\n"); fflush(stdout);
  install_keyboard();

  printf(" Loading Data\n"); fflush(stdout);
  packfile_password("allegator");
  load_data();
  packfile_password(NULL);

  printf(" Installing Timers\n"); fflush(stdout);
  install_timer();
  LOCK_VARIABLE(game_count);
  LOCK_VARIABLE(fps);
  LOCK_VARIABLE(frame_count);
  LOCK_FUNCTION(fps_counter);
  install_int(fps_counter,1000);
  fps=0;
  frame_count=0;
  install_int(game_counter,20);
  game_count=0;

  printf(" Installing Sound\n"); fflush(stdout);
  if (install_sound(DIGI_AUTODETECT, MIDI_AUTODETECT, NULL) != 0) {
    printf("Error initialising sound system\n%s\n", allegro_error);
  }

  printf(" Reserving Memory\n"); fflush(stdout);
  swap_screen = create_bitmap(320, 200);
  bkg_screen = create_bitmap(640, 200);
  view1 = create_sub_bitmap(swap_screen, 0, 0, 160, 200);
  view2 = create_sub_bitmap(swap_screen, 160, 0, 160, 200);

  set_gfx_mode(GFX_AUTODETECT,320,200,0,0);
  text_mode(-1);

// remove when cfg-file is written and loaded
  reset_keys();
}

void shutdown()
{
  fade_out(4);

  set_gfx_mode(GFX_TEXT, 0, 0, 0, 0);
  printf(CLEAR_SCREEN);
  printf(FG_BRIGHT_WHITE BG_GREEN);
  printf("█▓▒░  Alex the Allegator                       Copyright 1999  Johan Peitz  " FG_BLACK "░▒▓█\n");
  printf(FG_BLUE BG_DEFAULT " contact: d98peitz@dtek.chalmers.se                see readme.txt for more info" RESET "\n\n\a");
  unload_datafile(gfx);
  unload_datafile(snd);
  destroy_bitmap(swap_screen);
  destroy_bitmap(bkg_screen);
  destroy_bitmap(view1);
  destroy_bitmap(view2);
  allegro_exit();
}

void draw_screen(int p)
{
  int x,y,i;

  int screenx,sx,sxadj;
  int screeny,sy,syadj;

  // fps...
  frame_count++;

  // calculate which part of the screen that's supposed to be shown
  screenx=MIN(MAX(player[p].x-159,0),info.width_c-350);
  sx=screenx/24;
  sxadj=screenx-sx*24;

  screeny=MIN(MAX(player[p].y-90,0),info.height_c-200);
  sy=screeny/20;
  syadj=screeny-sy*20;

//  clear_to_color(swap_screen,108);      // change to image from parallax
  blit(bkg_screen,swap_screen,(screenx%1280)>>2,0,0,0,320,200);

  for(x=0;x<15;x++)
    for(y=0;y<11;y++)
      if(map[x+sx][y+sy].tile[0])
        draw_rle_sprite(swap_screen,gfx[TILE+map[x+sx][y+sy].tile[0]].dat,x*24-sxadj,y*20-syadj);

  for(i=0;i<=players;i++)
    if (player[i].alive)
      if (player[i].dir) draw_sprite_h_flip(swap_screen,gfx[HERO+player[i].image+i*5].dat,player[i].x-screenx-8,player[i].y-screeny-21);
      else draw_sprite(swap_screen,gfx[HERO+player[i].image+i*5].dat,player[i].x-screenx-8,player[i].y-screeny-21);

  draw_sprite(swap_screen,gfx[GIRL].dat,info.gx*24-screenx-8,info.gy*20-screeny-6);

  for(x=0;x<15;x++)
    for(y=0;y<11;y++)
      if(map[x+sx][y+sy].tile[1])
        draw_rle_sprite(swap_screen,gfx[TILE+map[x+sx][y+sy].tile[1]].dat,x*24-sxadj,y*20-syadj);

// show some hopefully interesting info
//  textprintf(swap_screen,font,0,0,31,"fps: %ld",fps);

  vsync();
  blit(swap_screen,screen,0,0,0,0,320,200);
}

void new_level()
{
  int i;

  load_map();
  for(i=0;i<=players;i++)
  {
    player[i].anim_count=0;
    player[i].x=info.startx*24+4;
    player[i].y=info.starty*20+15;
    player[i].xf=0;
    player[i].yf=0;
    player[i].dir=1;
    player[i].image=1;
    player[i].alive=1;
    player[i].jump=0;
  }

  draw_rle_sprite(bkg_screen,gfx[BKG].dat,0,0);
  draw_rle_sprite(bkg_screen,gfx[BKG].dat,320,0);
  play_midi(snd[M_LEVEL].dat,1);
  draw_screen(0);
  fade_in(gfx[PAL].dat,8);
}

void game_over()
{
  stop_midi();
  play_midi(snd[M_TITLE].dat,1);
}

void anim_player(int p)
{
  player[p].anim_count++;
  if (player[p].anim_count==4)
  {
    player[p].anim_count=0;
    player[p].image++;
    if (player[p].image==6) player[p].image=2;
  }
}

void epilogue()
{
  int ft=16;
  int wt=1000;

  clear(screen);
  textout_centre(screen,gfx[FNT_MARKER].dat,"Congratulations!",160,70,-1);
  fade_in(gfx[PAL].dat,ft);
  rest(wt);
  fade_out(ft);
  clear(screen);
  textout_centre(screen,gfx[FNT_MARKER].dat,"You completed",160,70,-1);
  fade_in(gfx[PAL].dat,ft);
  rest(wt);
  fade_out(ft);
  clear(screen);
  textout_centre(screen,gfx[FNT_MARKER].dat,"the demo!",160,70,-1);
  fade_in(gfx[PAL].dat,ft);
  rest(wt);
  fade_out(ft);
  clear(screen);
  textout_centre(screen,gfx[FNT_MARKER].dat,"Thanks for playing.",160,70,-1);
  fade_in(gfx[PAL].dat,ft);
  rest(2*wt);
  fade_out(ft);
}

void play()
{
  int ox,oy,i;
  int done=0;

  new_level();

  do {
    game_count=0;
    draw_screen(0);

    for(i=0;i<=players;i++)
    {
      if (player[i].alive)
      {
        player[i].xf=0; ox=player[i].x; oy=player[i].y;
        if (key[pkey[i].left]) { player[i].xf=-2; player[i].dir=0; anim_player(i); }
        if (key[pkey[i].right]) { player[i].xf=2; player[i].dir=1; anim_player(i); }
        if (!key[pkey[i].right] && !key[pkey[i].left]) player[i].image=1;
        player[i].x+=player[i].xf;

        player[i].spot[0]=is_ground(player[i].x-1,player[i].y+4);
        player[i].spot[1]=is_ground(player[i].x+9,player[i].y+4);
        if (player[i].spot[0] || player[i].spot[1]) player[i].x=ox;
        player[i].spot[0]=is_ground(player[i].x-1,player[i].y-8);
        player[i].spot[1]=is_ground(player[i].x+9,player[i].y-8);
        if (player[i].spot[0] || player[i].spot[1]) player[i].x=ox;
        player[i].spot[0]=is_ground(player[i].x-1,player[i].y-20);
        player[i].spot[1]=is_ground(player[i].x+9,player[i].y-20);
        if (player[i].spot[0] || player[i].spot[1]) player[i].x=ox;

        if (key[pkey[i].jump] && !player[i].jump) { player[i].yf=-20; player[i].jump=1; }
        if (!key[pkey[i].jump] && player[i].yf<0) player[i].yf=player[i].yf>>1;

        player[i].spot[0]=is_ground(player[i].x,player[i].y+4);
        player[i].spot[1]=is_ground(player[i].x+8,player[i].y+4);
        if (!player[i].spot[0] && !player[i].spot[1]) player[i].jump=1;

        if (player[i].jump) player[i].yf++;
        player[i].y+=player[i].yf>>2;
        player[i].spot[0]=is_ground(player[i].x-1,player[i].y-20);
        player[i].spot[1]=is_ground(player[i].x+9,player[i].y-20);
        if (player[i].spot[0] || player[i].spot[1]) { player[i].y=oy; player[i].yf=0; player[i].jump=1; }

        player[i].spot[0]=is_ground(player[i].x,player[i].y+4);
        player[i].spot[1]=is_ground(player[i].x+8,player[i].y+4);

        if (player[i].spot[0] || player[i].spot[1])
        {
          player[i].jump=0; player[i].yf=0;
          while (is_ground(player[i].x,player[i].y+4)) player[i].y--;
          while (is_ground(player[i].x+8,player[i].y+4)) player[i].y--;
        }

        if (player[i].x/24==info.gx && player[i].y/20==info.gy) done=i+1;
      }
    }

    while (!game_count);      // stop game from running like greased lightning

// shoot the screen :)
      if (key[KEY_PRTSCR]) take_screenshot();
// pause game
      if (key[KEY_PAUSE]) pause_game();
  } while(!key[KEY_ESC] && !done);

  fade_out(5);

  if (done) epilogue();

  game_over();
}

void show_help()
{
  printf("\nAlex the Allegator\n");
  printf("==================\n");
  fflush(stdout);
  printf("Commandline options:\n");
  printf("  -h        this text\n");
  printf("  -nointro  start game without intro\n");
  printf("\n");
  exit(1);
}

void intro()
{
  int ft=16;
  int wt=1000;

  set_palette(black_palette);
  clear(screen);
  play_midi(snd[M_TITLE].dat,1);
  rest(wt);
  set_palette(black_palette);
  clear(screen);
  textout_centre(screen,gfx[FNT_MARKER].dat,"Johan Peitz",160,70,-1);
  fade_in(gfx[PAL].dat,ft);
  rest(wt);
  fade_out(ft);
  clear(screen);
  textout_centre(screen,gfx[FNT_MARKER].dat,"in association with",160,70,-1);
  fade_in(gfx[PAL].dat,ft);
  rest(wt);
  fade_out(ft);
  clear(screen);
  draw_rle_sprite(screen,gfx[SHLOGO].dat,0,0);
  fade_in(gfx[SHPAL].dat,ft);
  rest(wt);
  fade_out(ft);
  clear(screen);
  textout_centre(screen,gfx[FNT_MARKER].dat,"presents",160,70,-1);
  fade_in(gfx[PAL].dat,ft);
  rest(wt);
  fade_out(ft);
  clear(screen);
  textout_centre(screen,gfx[FNT_MARKER].dat,"an uncomplete entry",160,70,-1);
  fade_in(gfx[PAL].dat,ft);
  rest(wt);
  fade_out(ft);
  clear(screen);
  textout_centre(screen,gfx[FNT_MARKER].dat,"known as",160,70,-1);
  fade_in(gfx[PAL].dat,ft);
  rest(wt);
  fade_out(ft);
}

int menu()
{
  int done=0;
  int pos=0;
  int x,y,pressed;

  play_midi(snd[M_TITLE].dat,1);
  clear(screen);
  for(x=0;x<14;x++)
    for(y=0;y<10;y++)
      draw_rle_sprite(screen,gfx[TILE003+random()%3].dat,x*24,y*20);

  draw_rle_sprite(screen,gfx[TITLE].dat,7,20);
  draw_rle_sprite(screen,gfx[ALLEGATOR].dat,62,40);

  textout_centre(screen,gfx[FNT_MARKER].dat,"1 PLAYER GAME",160,130,-1);
  textout_centre(screen,gfx[FNT_MARKER].dat,"2 PLAYER GAME",160,150,-1);
  textout_centre(screen,gfx[FNT_MARKER].dat,"EXIT",160,170,-1);

  blit(screen,swap_screen,0,0,0,0,320,200);

  fade_in(gfx[PAL].dat,8);

  pressed=0;
  while (!done)
  {
    if (key[KEY_DOWN] && !pressed) pos++;
    if (key[KEY_UP] && !pressed) pos--;
    pos=MAX(MIN(2,pos),0);
    if (key[KEY_UP] || key[KEY_DOWN]) pressed=1;
    if (!key[KEY_UP] && !key[KEY_DOWN]) pressed=0;

    vsync();
    blit(swap_screen,screen,0,0,0,0,320,200);
    draw_rle_sprite(screen,gfx[HAND].dat,50,140+pos*20);
    if (key[KEY_ENTER]) done=pos+1;
  }

  fade_out(8);
  return(done);
}

void main(int argc, char *argv[])
{
  int c;
  int nointro=0;

  for(c=1;c<argc;c++)
  {
    if (stricmp(argv[c], "-h")==0) show_help();
    if (stricmp(argv[c], "-nointro")==0) nointro=1;
    if (stricmp(argv[c], "--help")==0) show_help();
  }

  init();
  if (!nointro) intro();

  c=0;
  while(c!=3)
  {
    c=menu();
    if (c==1) { players=0; play(); }
    if (c==2) { players=1; play(); }
  }
}
END_OF_MAIN()
