ALEX THE ALLEGATOR      (c) Johan Peitz 1999
********************************************

This is my entry to the Allegro Speedhack 1999.
It's not finished. I didn't really have the time.
Here it is anyway. Have fun.

LINUX PORT
----------
This repository contains a Linux port of the game, now compatible with
Allegro 4 (but it should still work with Allegro 3 that the original used).

I haven't tested it, but it should also compile and work on MacOS and
Windows now, and even for DOS it should still somehow work (though you
would need an ANSI.SYS device driver now, and the header may contain
strange characters...)

RULE-O-MATICs
-------------
All fullfilled. Since genre was free I did a jump'n'run. The commandline
options are implemented and there's a BEL when exiting the game. (I know
this wasn't really imaginative but what the heck.) The number 29 is
inscribed with roman numbers on the wall in the secret cave in the right
bottom corner of the level. No Act of Dog is used.


HOW TO COMPILE
--------------
Since I know nothing of makefiles these lines do the trick:
  gcc -s -c alex.c -o alex.o
  gcc -s -o alex.exe alex.o -lalleg


THE GAME
--------
Explore the uneventful halfjungel in your attempt to find the beautiful Lola.
Use one or two allegators. No splitscreen is implemented so the view will
show where player one is only. Sorry about that. :(


STARTING THE GAME
-----------------
Type 'alex' with the options you want at the prompt. Navigate the menu
with the up and down arrows and select with ENTER.


KEYS
----
        pl1     pl2
walk    arrows  a,w,s,d
jump    r-ctrl  f

pause   PAUSE
quit    ESC


COMMANDLINE OPTIONS
-------------------
-h              shows helpscreen
-nointro        start game without intro


FACTS
-----
I have spent about 10 hrs (including gfx) on this game. I might finish it
but first I have to finish Operation Spacehog. Have faith.


THE AUTHOR
----------
Johan Peitz, d98peitz@dtek.chamlers.se
http://www.dtek.chalmers.se/~d98peitz


DISCLAIMER
----------
I do not accept responsibility for any effects, adverse or otherwise,
that this software may have on you, your computer, your sanity, your
dog, or anything else that you can think of. Use it at your own risk.
I have, however, never experienced any trouble with it.
                           



