/* Allegro datafile object indexes, produced by grabber v3.0 */
/* Datafile: c:\djgpp\fld\alex\gfx.dat */
/* Date: Mon Nov 29 00:35:26 1999 */
/* Do not hand edit! */

#define ALLEGATOR                        0        /* RLE  */
#define BKG                              1        /* RLE  */
#define FNT_MARKER                       2        /* FONT */
#define GIRL                             3        /* BMP  */
#define HAND                             4        /* RLE  */
#define HERO                             5        /* BMP  */
#define HEROa01                          6        /* BMP  */
#define HEROa02                          7        /* BMP  */
#define HEROa03                          8        /* BMP  */
#define HEROa04                          9        /* BMP  */
#define HEROa05                          10       /* BMP  */
#define HEROb01                          11       /* BMP  */
#define HEROb02                          12       /* BMP  */
#define HEROb03                          13       /* BMP  */
#define HEROb04                          14       /* BMP  */
#define HEROb05                          15       /* BMP  */
#define PAL                              16       /* PAL  */
#define SHLOGO                           17       /* RLE  */
#define SHPAL                            18       /* PAL  */
#define TILE                             19       /* RLE  */
#define TILE000                          20       /* RLE  */
#define TILE001                          21       /* RLE  */
#define TILE002                          22       /* RLE  */
#define TILE003                          23       /* RLE  */
#define TILE004                          24       /* RLE  */
#define TILE005                          25       /* RLE  */
#define TILE006                          26       /* RLE  */
#define TILE007                          27       /* RLE  */
#define TILE008                          28       /* RLE  */
#define TILE009                          29       /* RLE  */
#define TILE010                          30       /* RLE  */
#define TILE011                          31       /* RLE  */
#define TILE012                          32       /* RLE  */
#define TILE013                          33       /* RLE  */
#define TILE014                          34       /* RLE  */
#define TITLE                            35       /* RLE  */

