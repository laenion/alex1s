/*
  player functions
*/

typedef struct {
  int x,y;
  int dir;
  int xf,yf;
  int jump;
  int image;
  int anim_count;
  int spot[2];
  int alive;
  int lives;
  int energy;
} player_T;
player_T player[2];

// defined keys
typedef struct {
  int right,left;
  int up,down;
  int jump,action;
} key_struct;
key_struct pkey[2];


