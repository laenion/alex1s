/* ANSI escape codes for console color */
#define RESET           "\e[0m"
#define CLEAR_SCREEN    "\e[2J\e[1;1H" // also resets cursor
#define FG_BLACK        "\e[30m"
#define FG_GREEN        "\e[32m"
#define FG_BLUE         "\e[34m"
#define FG_WHITE        "\e[37m"
#define FG_BRIGHT_GREEN "\e[92m"
#define FG_BRIGHT_WHITE "\e[97m"
#define BG_GREEN        "\e[42m"
#define BG_CYAN         "\e[46m"
#define BG_DEFAULT      "\e[49m"
