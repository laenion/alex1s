/*
   Mapeditor

   FIX
   move map

*/

#define alleg_joystick_unused
#define alleg_vidmem_unused
#define alleg_flic_unused
#define alleg_math_unused
#define alleg_gui_unused
#define alleg_sound_unused
// #define alleg_timer_unused
// #define alleg_mouse_unused
// #define alleg_graphics_unused
// #define alleg_file_unused
// #define alleg_keyboard_unused
// #define alleg_gfx_driver_unused
// #define alleg_palette_unused
// #define alleg_datafile_unused

#include <stdio.h>
#include "allegro.h"
#include "ansicodes.h"
#include "gfx.h"

// Shrink the EXE size some
#ifdef GFX_DRIVER_VGA
BEGIN_GFX_DRIVER_LIST
  GFX_DRIVER_VGA
END_GFX_DRIVER_LIST
BEGIN_COLOR_DEPTH_LIST
  COLOR_DEPTH_8
END_COLOR_DEPTH_LIST
#endif

DATAFILE *gfx;
BITMAP *swap_screen;

#include "level.c"

// gamevariables
int cx,cy;                       // cursor pos
int ox,oy;                       // offset
int cb;                          // current block
int usemouse=1;
int onlayer[3]={1,1,0};
int clayer=0;                     // current layer 0=bg 1=fg

void loaddata()
{
  char buf[80];
  FILE *fp;

  strcpy(buf, "gfx.dat");
  gfx = load_datafile(buf);
  if (!gfx)
  {
    allegro_exit();
    printf("Not found: <%s>\n\n",buf);
    exit(1);
  }
}

void init()
{
  allegro_init();

  packfile_password("allegator");
  loaddata();

  install_keyboard();
  install_timer();
  install_mouse();

  swap_screen=create_bitmap(320,200);

  clear_map();
  set_gfx_mode(GFX_AUTODETECT, 320, 200, 0, 0);
  text_mode(-1);
  set_palette(gfx[PAL].dat);
}

void shutdown()
{
  fade_out(32);
  destroy_bitmap(swap_screen);
  unload_datafile(gfx);
  allegro_exit();
  set_gfx_mode(GFX_TEXT, 0, 0, 0, 0);
}

void drawscreen()
{
  int x,y;
  int mx=mouse_x;
  int my=mouse_y;

  clear_to_color(swap_screen,111);

  for(x=0;x<13;x++)
    for(y=0;y<9;y++)
    {
      if (onlayer[0] && map[x+ox][y+oy].tile[0])
        draw_rle_sprite(swap_screen,gfx[TILE+map[x+ox][y+oy].tile[0]].dat,x*24,y*20);
      if (onlayer[1] && map[x+ox][y+oy].tile[1])
        draw_rle_sprite(swap_screen,gfx[TILE+map[x+ox][y+oy].tile[1]].dat,x*24,y*20);
      if (onlayer[2] && map[x+ox][y+oy].type)
        rectfill(swap_screen,x*24,y*20,x*24+23,y*20+19,16);
    }

  draw_rle_sprite(swap_screen,gfx[TILE+cb].dat,cx*24,cy*20);
  rect(swap_screen,cx*24,cy*20,cx*24+23,cy*20+19,255);
  rect(swap_screen,mx-1,my-1,mx+1,my+1,255);

  hline(swap_screen,0,(info.height-oy)*20,319,255);
  vline(swap_screen,(info.width-ox)*24,0,199,255);
  rect(swap_screen,(info.startx-ox)*24+2,(info.starty-oy)*20+2,(info.startx-ox)*24+21,(info.starty-oy)*20+17,255);
  rect(swap_screen,(info.gx-ox)*24+4,(info.gy-oy)*20+4,(info.gx-ox)*24+19,(info.gy-oy)*20+15,254);

  textprintf(swap_screen,gfx[FNT_MARKER].dat,20,0,-1,"%d,%d",cx+ox,cy+oy);
  if (usemouse) textout(swap_screen,gfx[FNT_MARKER].dat,"mouse",2,170,-1);
  if (onlayer[0]) textout(swap_screen,gfx[FNT_MARKER].dat,"bgon",80,170,-1);
  if (onlayer[1]) textout(swap_screen,gfx[FNT_MARKER].dat,"fgon",150,170,-1);
  if (onlayer[2]) textout(swap_screen,gfx[FNT_MARKER].dat,"staton",220,170,-1);
  if (clayer) textout(swap_screen,gfx[FNT_MARKER].dat,"fg",2,140,-1);
  else textout(swap_screen,gfx[FNT_MARKER].dat,"bg",2,140,-1);

  vsync();
  blit(swap_screen,screen,0,0,0,0,320,200);
}

void autostat()
{
  int x,y;

  for(x=0;x<MAP_X;x++)
    for(y=0;y<MAP_Y;y++)
      map[x][y].type=get_type(map[x][y].tile[0]);
}

void overview()
{
  int x,y;

  rectfill(screen,10,10,MAP_X+11,MAP_Y+11,56);
  rect(screen,10,10,MAP_X+11,MAP_Y+11,31);

  for(x=0;x<MAP_X;x++)
    for(y=0;y<MAP_Y;y++)
      if (map[x][y].type) putpixel(screen,x+11,y+11,80);

  while(!key[KEY_ENTER]);
}

void edit()
{
  int kp,hold=0;

  cx=0; cy=0; cb=1;
  ox=0; oy=0;

  clear_keybuf();
  do {
    if (key[KEY_TAB]) rest(50);
    drawscreen();
    kp=0; if (keypressed()) kp=readkey()>>8;
    if (usemouse)
    {
      cx=(mouse_x)/24;
      cy=(mouse_y)/20;
    }
    if (kp==KEY_UP) cy--;
    if (kp==KEY_DOWN) cy++;
    if (kp==KEY_LEFT) cx--;
    if (kp==KEY_RIGHT) cx++;
    if (usemouse)
    {
      if (mouse_x==319) ox++;
      if (mouse_x==0) ox--;
      if (mouse_y==199) oy++;
      if (mouse_y==0) oy--;
    }
    else
    {
      if (cx==13) ox++;
      if (cx<0) ox--;
      if (cy==9) oy++;
      if (cy<0) oy--;
    }
    ox=MIN(MAP_X-13,MAX(ox,0));
    oy=MIN(MAP_Y-9,MAX(oy,0));
    cx=MIN(12,MAX(cx,0));
    cy=MIN(8,MAX(cy,0));
    if (key[KEY_1]) clayer=0;
    if (key[KEY_2]) clayer=1;
    if (!key[KEY_LSHIFT])
    {
      if (kp==KEY_P || (mouse_b & 2)) cb=map[cx+ox][cy+oy].tile[clayer];
      if (kp==KEY_H) hold++; if (hold==2) hold=0;
      if (kp==KEY_SPACE || hold || (mouse_b & 1))
      {
        map[cx+ox][cy+oy].tile[clayer]=cb;
        if (!clayer) map[cx+ox][cy+oy].type=get_type(cb);
      }
    }
    else
    {
      if (mouse_b & 1) map[cx+ox][cy+oy].type=get_type(cb);
      if (mouse_b & 2) map[cx+ox][cy+oy].type=0;
    }
    if (kp==KEY_END)
    {
      info.width=ox+cx+1;
      info.height=oy+cy+1;
    }
    if (kp==KEY_HOME)
    {
      info.startx=ox+cx;
      info.starty=oy+cy;
    }
    if (kp==KEY_INSERT)
    {
      info.gx=ox+cx;
      info.gy=oy+cy;
    }
    if (kp==KEY_O) overview();
    if (kp==KEY_F1) autostat();
    if (kp==KEY_F10) clear_map();
    if (kp==KEY_Z) cb--;
    if (kp==KEY_X) cb++;
    if (cb>NUM_BLOCKS) cb=0;
    if (cb<0) cb=NUM_BLOCKS;

    if (key[KEY_ALT])
    {
//      if (key[KEY_A]) save_map_as();
      if (key[KEY_S]) save_map();
      if (key[KEY_L]) load_map();
      if (key[KEY_N]) name_level();
    }
    else
    {
      if (kp==KEY_M && usemouse) usemouse=0;
      else if (kp==KEY_M && !usemouse) usemouse=1;
      if (kp==KEY_F && onlayer[1]) onlayer[1]=0;
      else if (kp==KEY_F && !onlayer[1]) onlayer[1]=1;
      if (kp==KEY_B && onlayer[0]) onlayer[0]=0;
      else if (kp==KEY_B && !onlayer[0]) onlayer[0]=1;
      if (kp==KEY_S && onlayer[2]) onlayer[2]=0;
      else if (kp==KEY_S && !onlayer[2]) onlayer[2]=1;
    }

  } while(kp!=KEY_ESC);
}

void main(int argc, char *argv[])
{
  int c;

  init();

  for (c=1;c<argc;c++)
  {
    if (stricmp(argv[c], "/kbd")==0) usemouse=0;
  }

  edit();
}
END_OF_MAIN()
