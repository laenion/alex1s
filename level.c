/*
  levelfunctions
*/

#define NUM_BLOCKS      15
#define MAP_X          128
#define MAP_Y          128

typedef struct {
   unsigned char tile[2];       // each square on the map has an id to what
   unsigned char type;          // image to show and if its walkable or not
} mappos_T;

typedef struct {
   unsigned char startx,starty; // hero start
   unsigned char width,height;  // mapsize
   char name[40];               // mapname
   int width_c,height_c;        // mapsize in pixels
   unsigned char gx,gy;         // goal
} mapinfo_T;

// gamevariables
mappos_T map[MAP_X][MAP_Y];        // the map
mapinfo_T info;                    // the map info

void load_map()
{
  FILE *fp;

  fp=fopen("test.map","rb");
  fread(map,sizeof(map),1,fp);
  fread(&info,sizeof(info),1,fp);
  fclose(fp);
}

void save_map()
{
  FILE *fp;

  info.width_c=info.width*24;
  info.height_c=info.height*20;

  fp=fopen("test.map","wb");
  fwrite(map,sizeof(map),1,fp);
  fwrite(&info,sizeof(info),1,fp);
  fclose(fp);
}

void clear_map()
{
  int x,y;

  for(x=0;x<MAP_X;x++)
    for(y=0;y<MAP_Y;y++)
    {
      map[x][y].tile[0]=0;
      map[x][y].tile[1]=0;
      map[x][y].type=0;
    }
  info.width=MAP_X;
  info.height=MAP_X;
  info.startx=2;
  info.starty=6;
  info.gx=10;
  info.gy=10;
  strcpy(info.name,"new level");
}

int get_type(int tile)
{
  if (tile==0) return(0);
  if (tile>0 && tile<4) return(1);

  return(0);
}

int is_ground(int x, int y)
{
  return(get_type(map[x/24][y/20].type));
}

void name_level()
{
  set_gfx_mode(GFX_TEXT, 0, 0, 0, 0);
  remove_keyboard();
  printf("\n\n\n" FG_BRIGHT_WHITE BG_CYAN "%s\n",info.name);
  printf(FG_BRIGHT_WHITE BG_GREEN "Levelname: ");
  gets(info.name);
  install_keyboard();
  set_gfx_mode(GFX_AUTODETECT,320,200,0,0);
  set_palette(gfx[PAL].dat);
}

